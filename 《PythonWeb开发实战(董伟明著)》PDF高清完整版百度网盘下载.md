![输入图片说明](121.jpg)

### 内容简介

![输入图片说明](6.png)

这本书涵盖了Web开发的方方面面，可以分为如下部分：

使用最新的Flask 0.11进行Web开发，学习Jinja2、Flask-Script、Flask-Migrate、Flask-Security、Flask-Assets等8种常用扩展，帮助读者理解Flask 的 优秀设计（上下文、BluePrint等），最后利用Mako、Flask_mako、SQLAlchemy、Pillow等技术实现一个豆瓣一个真实的服务。

阐述笔者对REST的理解，并提出一些设计API的注意事项，最后通过jQuery和fetch实现使用Ajax的例子，让读者了解如何让前后端通信。

对Python应用服务器，Web服务器、缓存系统、键值对数据库等技术的选型和使用方法，最后演示大型网站架构及其重要组件的用意。

使用Fabric、SaltStack、Ansible、Supervisor、Graphite等做系统管理，并演示一个通过最新的Sentry 8演示如何收集应用错误信息。

测试和持续集成，最后使用最新的Buildbot 0.9实现一个Github项目的持续集成。

深入RabbitMQ和Celery的原理和使用方法，最后分享笔者使用的进阶实践。

服务化及豆瓣服务化实践。

详细讲解豆瓣工程师都在用的DPark，包含安装、环境配置、使用和框架化分析uv&pv，接着将展示几个笔者实际工作中的数据报表需求，并讲解如何用Pandas做数据可视化。

深入IPython和Jupyter Notebook这两个工具，并分享在豆瓣对应的实践。

从获取Linux服务器的相关情况、性能测试、分析Python程序性能瓶颈三个方面展示对应的工具及使用方法。

以抓取微信公众号文章为主线，分别使用多线程、多进程、Gevent、Future和asyncio这5种编程方式完成不同阶段的爬取任务，也深入地分析在它们之间如何选择。

Python进阶和Web项目经验。

### 作者简介


作者是豆瓣条目组高级产品开发工程师，主要负责豆瓣读书（对，你没有看错，就是这个网站）、电影、音乐、东西等产品线。从2011年开始接触Python, 从运维、运维开发到现在的Web开发，积累了丰富的运维和开发经验，这本书将作者这些年使用Python进行Web开发，对各方面知识的理解和积累的经验进行梳理和总结。
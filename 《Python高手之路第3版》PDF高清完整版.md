![输入图片说明](121.jpg)

### 内容简介

![输入图片说明](23221.png)

这不是一本常规意义上Python的入门书。这本书中没有Python关键字和for循环的使用，也没有细致入微的标准库介绍，而是完全从实战的角度出发，对构建一个完整的Python应用所需掌握的知识进行了系统而完整的介绍。更为难得的是，本书的作者是开源项目OpenStack的PTL（项目技术负责人）之一，因此本书结合了Python在OpenStack中的应用进行讲解，非常具有实战指导意义。

本书从如何开始一个新的项目讲起，首先是整个项目的结构设计，对模块和库的管理，如何编写文档，进而讲到如何分发，以及如何通过虚拟环境对项目进行测试。此外，本书还涉及了很多高级主题，如性能优化、插件化结构的设计与架构、Python 3的支持策略等。本书适合各个层次的Python程序员阅读和参考。

### 作者简介


Julien Danjou 具有12年从业经验的自由软件黑客。拥有多个开源社区的不同身份：Debian开发者、Freedesktop贡献者、GNU Emacs提交者、awesome窗口管理器的创建者以及OpenStack Ceilometer项目的技术主管。近年，他经常使用Python，尤其是在参与了OpenStack（云计算平台）的开发之后。在此期间，他有机会与许多杰出的黑客一起工作。